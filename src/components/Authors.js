"use client";

import React, { useState, useEffect } from "react";

export default function Authors({ authors }) {
  const [randomAuthor, setRandomAuthor] = useState(null);
  const [winner, setWinner] = useState(null);

  useEffect(() => {
    const interval = setInterval(() => {
      setRandomAuthor(authors[Math.floor(Math.random() * authors.length)]);
    }, 100);

    const timeout = setTimeout(() => {
      clearInterval(interval);
      setRandomAuthor(null);
      setWinner(authors[Math.floor(Math.random() * authors.length)]);
    }, 10000);

    return () => {
      clearInterval(interval);
      clearTimeout(timeout);
    };
  }, [authors]);

  return (
    <ul>
      {authors.map((author) => (
        <li
          key={author.id}
          className={`py-2 px-4 rounded-md ${
            author.id === randomAuthor?.id
              ? "bg-orange-200 text-orange-800"
              : author.id === winner?.id
              ? "bg-green-200 text-green-800"
              : "bg-gray-100"
          }`}
        >
          <div className="flex items-center">
            <img
              src={author.avatar_url}
              alt={author.name}
              className="w-8 h-8 rounded-full mr-2"
            />
            <span className="font-semibold">{author.name}</span>
          </div>
        </li>
      ))}
    </ul>
  );
}
