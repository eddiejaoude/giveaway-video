import Authors from "@/components/Authors";

export default async function Home() {
  // call gitlab's api to get a list of comments on an issue use fetch
  const fetchComments = async (projectId, issueId) => {
    let comments = [];
    try {
      const response = await fetch(
        `https://gitlab.com/api/v4/projects/${projectId}/issues/${issueId}/notes?per_page=100`,
        {
          headers: {
            "Private-Token": process.env.GITLAB_TOKEN,
          },
          cache: "no-store",
        }
      );
      comments = await response.json();
    } catch (e) {
      console.log("ERROR:", e);
    }

    return comments;
  };

  // ---------- CONFIG --------
  const issueId = 15;
  const projectId = 58236098;
  // ---------- CONFIG --------

  const data = await fetchComments(projectId, issueId);

  // filter for unique authors only and keep all the objects
  const uniqueAuthors = data
    .filter((comment) => !comment.system)
    .map((comment) => comment.author)
    .filter(
      (author, index, self) =>
        self.findIndex((a) => a.id === author.id) === index
    );

  return (
    <>
      <h2>
        Issue number {issueId} with unique authors of {uniqueAuthors.length}
      </h2>
      <Authors authors={uniqueAuthors} />
    </>
  );
}
